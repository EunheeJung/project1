# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/20160595_eunheeJung_video.mp4)
* Description of projects and notes in README.md (this file). 

### Bitbucket link and Video link
* Bitbucket : https://bitbucket.org/EunheeJung/project1/src/master/
* Video : https://bitbucket.org/EunheeJung/project1/src/master/Video/
	
	
### Discription

* this is basic mario game. mario moves while killing goomba and collecting coins. Also, mario can crash the blocks. 

* user can see the score and time. 

* I tried to implement all the functions, but it's not yet completed. I'm planning to modify further while doing project2. 

### Reference

* mario pictures reference : https://www.youtube.com/watch?v=wsv0jxhSXPM

    this reference page has the persons's code, but I swear that I never read the code. I'm sure you can believe me when you compare two files! (Which I didn't ever read the persons's code.) I only got png and gif files from the page. 

* my friend Yeji Han answered my basic questions of processing coding. 

