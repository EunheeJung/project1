class Goomba
{
	private float x, y, moveMin, moveMax;
	private float velX=10;
	private float velY=5;
	private PImage [] goombawalk = new PImage [2];
	private PImage [] goombadie = new PImage [3];

	Goomba (float x, float y, float moveMin, float moveMax)
	{
		this.x = x;
		this.y = y;
		this.moveMin = moveMin;
		this.moveMax = moveMax;

		for (int i=0; i< 2; i ++)
		{
			goombawalk[i] = loadImage("characters/Goomba/000"+i+".gif");
		}

		for (int i=1; i< 4; i ++)
		{
			goombadie[i-1] = loadImage("characters/Goomba/000"+i+".gif");
		}
	}

	public float getX() {return x;}
	public float getY() {return y;}

	void backgroundmove()
	{
		moveMin-=15;
		moveMax-=15;
	}

	void move()
	{
		imageMode(CENTER);
		image(goombawalk[frameCount%2],x,y);
		
		if (x<moveMin)
		{
			velX = (-1)*velX;
		}
		else if (x>moveMax)
		{
			velX = (-1)*velY;
		}
		x += velX;
	}

	void draw()
	{
		imageMode(CENTER);
		image(goombawalk[frameCount%2],x,y);
	}

	void die()
	{
		imageMode(CENTER);
		image(goombadie[frameCount%3],x,y);
	}
    
}