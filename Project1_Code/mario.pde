class Mario extends Thread
{
    // Mario's location and velocity
	private float x, y;
    private float velocityX; 
	private float velocityY = 25;
	private float g = 1;

    // Mario's condition
    private boolean dir = true;
    private boolean stuck = false;

    // Animation
	private PImage[] walkingimgs = new PImage[4];
	private PImage[] postureimgs = new PImage[3];
	private PImage[] dieimgs = new PImage[4];
    
    private float landingY;

	Mario (float x, float y)
	{
		this.x = x;
		this.y = y;

		for (int i=0; i < 4; i++)
		{
			walkingimgs[i] = loadImage("characters/mario/walk/000"+i+".gif");
            dieimgs[i] = loadImage("characters/mario/die/000"+i+".gif");
		}

		for (int i=0; i < 3; i++)
		{
			postureimgs[i] = loadImage("characters/mario/00"+i+".gif");
		}
	}

	public float getX() { return x; }
	public float getY() { return y; }
    public boolean isStuck() { return stuck; }

	public void draw()
	{
		imageMode(CENTER);
		image(postureimgs[2], x, y);
	}

    public void stopMove()
    {
        imageMode(CENTER);
		image(walkingimgs[frameCount%4], x, y);
    }

    public void stopflipMove()
    {
        imageMode(CENTER);
        pushMatrix();
		translate(width/2, 0);
		scale(-1, 1);
		translate(-width/2, 0);
		image(walkingimgs[frameCount%4], 1280-x, y);
		popMatrix();
    }

	public void flip()
	{
		imageMode(CENTER);
		
		pushMatrix();
		translate(width/2, 0);
		scale(-1, 1);
		translate(-width/2, 0);
		image(walkingimgs[frameCount%4], 1280-x, y);
		x -= 10;
		popMatrix();

        dir = false;
	}

	public void move()
	{
		try
		{
			imageMode(CENTER);
			image(walkingimgs[frameCount%4], x, y);
		}
		catch(Exception e){
			return;
		}
		x += 5;
        dir = true;
	}

	public void jump(float landY)
	{
		imageMode(CENTER);

		if(velocityY > 0){
			image(postureimgs[1], x, y);
		}
		else{
			image(postureimgs[0],x,y);
		}
		
		y -= velocityY;
		velocityY -= g;
		if(y>landY)
		{
			y=landY;
			velocityY = 20;
			
			check3=false;
		}
	}

    public void fall()
    {
        imageMode(CENTER);
        image(postureimgs[0],x,y);
        
        while (y<height*4.1/5)
        {
            y += velocityY;
        }
        y=height*4.1/5;
    }

    public void flipfall()
    {
        imageMode(CENTER);
        pushMatrix();
		translate(width/2, 0);
		scale(-1, 1);
		translate(-width/2, 0);
		image(postureimgs[0],x,y);
		while (y<height*4.1/5)
        {
            y += velocityY;
        }
        y=height*4.1/5;
		popMatrix();
        
        
    }

    public void setlandingY(float y)
    {
       this.landingY = y;
    }

    public void setX(float x)
    {
        this.x = x; 
    }

    public void setY (float y)
    {
        this.y = y;
    }

	public void moveJump(float landY)
	{
        dir = true;

		imageMode(CENTER);
		if (velocityY > 0){
			image(postureimgs[1],x,y);
		}
		else{
			image(postureimgs[0],x,y);
		}
		
		y -= velocityY;
		x += 5;
		velocityY -= g;
		if (y > landY)
		{
            println("b", y, landY);
			y = landY;
			velocityY = (-1) * velocityY;
			check3 = false;
		}
	}

	public void flipJump(float landY)
	{
        dir = false;
		imageMode(CENTER);
		
		pushMatrix();
		translate(width/2,0);
		scale(-1,1);
		translate(-width/2,0);
		

		if(velocityY>0){
			image(postureimgs[1],1280-x, y);
		}
		else{
			image(postureimgs[0],1280-x, y);
		}
		
		y -= velocityY;
		x -= 10;
		velocityY -= g;
        float a = y+ 50;
		if(a > landY)
		{
			y = landY;
			velocityY = (-1) * velocityY;
			check3 = false;
		}
        else{
        }
        

		popMatrix();
	}

	void hitPipe(float lowbX, float highbX, float highbY)
	{
        if (this.getX() > lowbX && this.getX() < highbX)
            stuck = true;
        else
            stuck = false;
	}

	void hitBlock()
	{

	}


	void die()
	{
        imageMode(CENTER);
		image(dieimgs[frameCount%4], x, y);
	}

    void reset()
    {
        this.x = width/2;
        this.y = height*4.1/5;
    }
}