class Background
{
	private int MAXELEMENT = 6;
	private PImage[] backgrounds = new PImage[9];
	private int x, y;
	private float[][] pipes = new float[MAXELEMENT][2];
    private int howMove;

	Background(int x, int y)
	{
		this.x=x;
		this.y=y;
		
		for (int i =0; i < 9; i++)
		{
			backgrounds[i] = loadImage("background/LevelPiece"+(i+1)+".png");
		}

		pipes[0][0] = width + width/8;
        pipes[0][1] =  height * (4.1/5 - 2.0/16);
		pipes[1][0] = width + width/2 - 38.0;
        pipes[1][1] = height * (4.1/5 - 3.0/16);     
		pipes[2][0] = width + width*5/7 + 60.0;
        pipes[2][1] = height * (4.1/5 - 3.5/16);      
		pipes[3][0] = width*2 + width/8 + 30.0;
        pipes[3][1] = height * (4.1/5 - 3.5/16); 
		pipes[4][0] = width*6 + width*1.2/3 - width *6.0/7 + width /4 -80.0;
        pipes[4][1] = height * (4.1/5 - 2.0/16);    
		pipes[5][0] = width*6 + width/16 + width / 5;
        pipes[5][1] =height * (4.1/5 - 2.0/16); 
	}

	void draw()
	{
		imageMode(CORNER);
		for (int i =0; i<9; i++)
		{
			image(backgrounds[i], x+ 1280*i, y, 1280, 720);
		}
	}

    int gethowMove() {return howMove;}



	void move()
	{
		x-=15;
        howMove +=15;
	}

	void pipeMove()
	{
		for (int i = 0; i < 6; i++)
        {
            pipes[i][0] -= 13;
        }
	}

    void pipeReset()
    {
        pipes[0][0] = width + width/8;
        pipes[0][1] =  height * (4.1/5 - 1.7/16);
		pipes[1][0] = width + width/2;
        pipes[1][1] = height * (4.1/5 - 1.5/16);     
		pipes[2][0] = width + width*6/7;
        pipes[2][1] = height * (4.1/5 - 1.5/16);      
		pipes[3][0] = width*2 + width/2;
        pipes[3][1] = height * (4.1/5 - 1.5/16); 
		pipes[4][0] = width*6 + width*1.2/3;
        pipes[4][1] = height * (4.1/5 - 1.5/16);    
		pipes[5][0] = width*7 + width/16;
        pipes[5][1] =height * (4.1/5 - 1.5/16); 
    }

    float[][] getPipe() { return pipes; }

    void reset()
    {
        this.x =0;
        this.y =0;
        pipeReset();
        
    }
}