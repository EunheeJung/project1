float width = 1280;
float height = 720;
float ground = height*4.1/5;
Mario mario, mariofliped;
Goomba goomba;

Background bg;
boolean check1 = false;
boolean check2 = false;
boolean check3 = false;
boolean stuck = false; 
boolean stucks[];
boolean start = true;

String tIndicater;
String sIndicater;

int a;
int score = 000000;
int time = 0;
int savedtime=0;
float highbX; //width * (2.0/3 + 1.0/16);
float lowbX; //(2.0/3 - 2.0/16) * width;
float highbY; // height * (4.1/5 - 1.5/16);
float[][] pipes;

PImage title;
PFont font;

ArrayList<Goomba> goombas = new ArrayList<Goomba>();
ArrayList<Coin> coins = new ArrayList<Coin>();
ArrayList<Block> blocks = new ArrayList<Block>();
ArrayList<Elements> elements = new ArrayList<Elements>();

class Game
{
    
    Game()
    {
        font = createFont("Super-Mario-World.ttf", 32);
        mario = new Mario(width/2, height*4.1/5);
        mariofliped = new Mario(width/3, height*4.1/5);
        bg = new Background(0,0);
        pipes = bg.getPipe();
        stucks = new boolean[6];
        title = loadImage("title.png");
        time = 0;
        savedtime = millis();
        

        goombas.add(new Goomba(width* 3.0/4, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width*2.0/4, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width+ width/2, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width + width*5/7 + 60.0, height*4.2/5, width + width*5/7 + 60.0, width*2 + width/8 + 30.0));
        goombas.add(new Goomba(width*3, height*4.2/5, width*3 ,width*(3+1.0/4)));
        goombas.add(new Goomba(width*3+ width/2, height*4.2/5, width*3 ,width*(3+1.0/4)));
        goombas.add(new Goomba(width*4, height*4.2/5, width*4,width*(4+2.0/3)));

        elements.add(new Block(width/3, height/3));
        elements.add(new Item(width/3, height/3));

        coins.add(new Coin(width/3, height*2.0/3));
        coins.add(new Coin(width/3 + 30 , height*2.0/3));
        

        for (int i = 0; i < 8; i ++)
        {
            coins.add(new Coin(width + width/3 +30*i, height*1.2/2));
        }
        
        for (int i = 0; i < 8; i ++)
        {
            coins.add(new Coin(4 * width + width/3 +30*i, height*1.2/2));
        }

        blocks.add(new Block(width*2.0/3, height/2));
        blocks.add(new Block(width*2.0/3 + 50, height/2));

        for (int i = 0; i < 4; i ++)
        {
            blocks.add(new Block(width + 50*i, height*1.7/3));
        }
        

	
    }

    void setCheck1(boolean b) {check1 = b;}
    void setCheck2(boolean b) {check2 = b;}
    void setCheck3(boolean b) {check3 = b;}

    void updateAndDraw()
    {
        background(93,134,196);
        imageMode(CORNER);

        tIndicater = nf(time, 6);
        sIndicater = nf(score, 6);
        fill(255);
        textFont(font, 28);
        text ("MARIO", 30, 50);
        text ("WORLD", 900, 50);
        text ("TIME", 1100, 50);
        text (tIndicater, 1100, 80);
        text (sIndicater , 30, 80);

        if(millis()-savedtime >= 1000)
        {
            time += 1;
            savedtime = millis();
        }

        if (start)
        {
            image(title, 0, 0);
            fill(255);
            textFont(font, 28);
            
            text ("1 PLAYER GAME", 500, 420);
            text ("2 PLAYER GAME", 498, 470);
        }
        
        bg.draw();

        if(goombas.size() != 0)
        {
            for (int i = 0; i < goombas.size(); i++)
            {
                Goomba e = goombas.get(i);
                
                e.move();
            }
        }
        
        if(coins.size() != 0)
        {
            for (int i = 0; i < coins.size(); i++)
            {
                Coin e = coins.get(i);
                e.drawElement();
            }
        }

        if(blocks.size() != 0)
        {
            for (int i = 0; i < blocks.size(); i++)
            {
                Block e = blocks.get(i);
                e.drawElement();
            }
        }
        
        
        
        // mario facing blocks
        for ( int i = 0 ; i < 6 ; i++)
        {
            mario.hitPipe(pipes[i][0] - 2.0/16*width, pipes[i][0] + 1.0/16*width , pipes[i][1]);
            stucks[i] = mario.isStuck();
            if (stucks[i]==true)
            {
                a = i;
                break;
            }
        }
        
        float highbX = pipes[a][0] - 0.6/16*width;
        float lowbX = pipes[a][0] - 2.0/16*width; 
        float highbY = pipes[a][1];
        mario.hitPipe(lowbX, highbX, highbY);

        if (check1 == true && check3 == false)
        {
            if (mario.isStuck() && mario.getY() > highbY)
            {
                mario.setX(lowbX);
                mario.stopMove();
            }

            else if (mario.getX() == lowbX)
            {
                mario.stopMove();
            }

            else if(mario.isStuck() && mario.getY() < ground )
            {
                mario.setY(highbY);
                mario.move();
                bg.move();
                lowbX -= 15;
                highbX -=15; 
                bg.pipeMove();
                if(goombas.size() != 0)
                {
                    for (int i = 0; i < goombas.size(); i++)
                    {
                        Goomba g = goombas.get(i);
                        g.backgroundmove();
                    }
                }

                if(coins.size() != 0)
                {
                    for (int i = 0; i < coins.size(); i++)
                    {
                        Coin e = coins.get(i);
                        e.backgroundmove();
                    }
                }

                if(blocks.size() != 0)
                {
                    for (int i = 0; i < blocks.size(); i++)
                    {
                        Block e = blocks.get(i);
                        e.backgroundmove();
                    }
                }
            }

            else if (!mario.isStuck() && mario.getY() < ground )
            {
                mario.fall();
            }
            else {
                mario.move();
                bg.move();
                lowbX -= 15;
                highbX -=15; 
                bg.pipeMove();
                if(goombas.size() != 0)
                {
                    for (int i = 0; i < goombas.size(); i++)
                    {
                        Goomba g = goombas.get(i);
                        g.backgroundmove();
                    }
                }
                

                if(coins.size() != 0)
                {
                    for (int i = 0; i < coins.size(); i++)
                    {
                        Coin e = coins.get(i);
                        e.backgroundmove();
                    }
                }

                if(blocks.size() != 0)
                {
                    for (int i = 0; i < blocks.size(); i++)
                    {
                        Block e = blocks.get(i);
                        e.backgroundmove();
                    }
                }
                
            }
        }

        else if(check2 == true && check3 ==false)
        {
            if (mario.isStuck() && mario.getY() > highbY)
            {
                mario.setX(highbX);
                mario.stopflipMove();
            }
            else if (mario.getX() == highbX)
            {
                mario.stopflipMove();
            }
            else if (!mario.isStuck() && mario.getY() < ground )
            {
                mario.flipfall();
            }

            else {
                mario.flip();
            }
        }

        else if (check1 == true && check3 ==true)
        {
            if (mario.isStuck())
            {
                if(mario.getY() < highbY)
                {
                    mario.moveJump(highbY);
                    lowbX -= 15;
                    highbX -=15; 
                    bg.move();
                    bg.pipeMove();
                    if(goombas.size() != 0)
                    {
                        for (int i = 0; i < goombas.size(); i++)
                        {
                            Goomba g = goombas.get(i);
                            g.backgroundmove();
                        }
                    }

                    if(coins.size() != 0)
                    {
                        for (int i = 0; i < coins.size(); i++)
                        {
                            Coin e = coins.get(i);
                            e.backgroundmove();
                        }
                    }

                    if(blocks.size() != 0)
                    {
                        for (int i = 0; i < blocks.size(); i++)
                        {
                            Block e = blocks.get(i);
                            e.backgroundmove();
                        }
                    }
                }
                mario.stopMove();
            }
            else {
                mario.setlandingY(height*4.1/5);
                mario.moveJump(height*4.1/5);
                lowbX -= 15;
                highbX -=15; 
                bg.move();
                bg.pipeMove();
                
                if(goombas.size() != 0)
                {
                    for (int i = 0; i < goombas.size(); i++)
                    {
                        Goomba g = goombas.get(i);
                        g.backgroundmove();
                    }
                }

                if(coins.size() != 0)
                {
                    for (int i = 0; i < coins.size(); i++)
                    {
                        Coin e = coins.get(i);
                        e.backgroundmove();
                    }
                }

                if(blocks.size() != 0)
                {
                    for (int i = 0; i < blocks.size(); i++)
                    {
                        Block e = blocks.get(i);
                        e.backgroundmove();
                    }
                }
            }

        }

        else if(check2 == true && check3 == true)
        {
            
            if (mario.isStuck())
            {
                if(mario.getY() < highbY)
                {
                    mario.flipJump(highbY);
                }
                mario.stopMove();
            }
            else {
                mario.flipJump(height*4.1/5);
                
            }
        }

        else if (check3 ==true)
        {
            if (mario.isStuck())
            {
                if(mario.getY() < highbY)
                {
                    mario.jump(highbY);
                }
                mario.stopMove();
            }
            mario.jump(height*4.1/5);
            
        }
        
        else {mario.draw();}

        
        try {
            for (int i = 0; i < coins.size(); i++)
            {
                Coin g = coins.get(i);
                marioCoin(i);
            }
            for (int i = 0; i < blocks.size(); i++)
            {
                Block g = blocks.get(i);
                marioBlock(i);
            }
            
        } catch (Exception e) {
        }

        try {
            for (int i = 0; i < goombas.size(); i++)
            {
                
                Goomba g = goombas.get(i);
                marioKillGoomba(i);
            }
        } catch (Exception e) {
            
        }

        try {
            for (int i = 0; i < goombas.size(); i++)
            {
                
                Goomba g = goombas.get(i);
                marioGoomba(i);
            }
        } catch (Exception e) {
            
        }
        endGame();
        
    }

    void endGame()
    {
        if(bg.gethowMove() > width * 7)
        {
            fill(255);
            textFont(font, 40);
            text ("THE END", width/2 - 100, height/2);

        }
    }

    void resetGame()
    {
        bg.reset();
        mario.reset();
        
        goombas.clear();
        elements.clear();
        coins.clear();
        blocks.clear();

        goombas.add(new Goomba(width* 3.0/4, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width*2.0/4, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width+ width/2, height*4.2/5, width/3,width*2/3));
        goombas.add(new Goomba(width + width*5/7 + 60.0, height*4.2/5, width + width*5/7 + 60.0, width*2 + width/8 + 30.0));
        goombas.add(new Goomba(width*3, height*4.2/5, width*3 ,width*(3+1.0/4)));
        goombas.add(new Goomba(width*3+ width/2, height*4.2/5, width*3 ,width*(3+1.0/4)));
        goombas.add(new Goomba(width*4, height*4.2/5, width*4,width*(4+2.0/3)));

        elements.add(new Block(width/3, height/3));
        elements.add(new Item(width/3, height/3));

        coins.add(new Coin(width/3, height*2.0/3));
        coins.add(new Coin(width/3 + 30 , height*2.0/3));

        for (int i = 0; i < 8; i ++)
        {
            coins.add(new Coin(width + width/3 +30*i, height*1.2/2));
        }
        
        for (int i = 0; i < 8; i ++)
        {
            coins.add(new Coin(4 * width + width/3 +30*i, height*1.2/2));
        }

        blocks.add(new Block(width*2.0/3, height/2));
        blocks.add(new Block(width*2.0/3 + 50, height/2));

        for (int i = 0; i < 4; i ++)
        {
            blocks.add(new Block(width + 50*i, height*1.7/3));
        }
    }

    void marioKillGoomba (int i)
    {
        Goomba q = goombas.get(i);
        if( abs(mario.getX()-q.getX()) < 5 && mario.getY() == ground)
        {
            q.die();
            goombas.remove(i);
        }
    }

    void marioGoomba(int i)
    {
        Goomba q = goombas.get(i);
        if(abs(mario.getX()-goomba.getX()) < 50 && abs(goomba.getY()-mario.getY()) <50 && mario.getY() < ground) //abs(mario.getX()-goomba.getX()) < 50 && abs(goomba.getY()-mario.getY()) <50 && mario.getY() < ground
        {
            mario.die();
            resetGame();
        }
    }

    void marioCoin(int i)
    {
        Coin c = coins.get(i);
        if(abs(mario.getX()-c.getX()) < 30 && abs(mario.getY()-c.getY()) <30)
        {
            
            coins.remove(i);
            score += 100;
        }
    }

    void marioBlock(int i)
    {
        Block b = blocks.get(i);
        if(abs(mario.getX()-b.getX()) < 25 && abs(mario.getY()-b.getY()) <25)
        {
            b.crash();

            blocks.remove(i);
            score += 50;
        }
    }
}