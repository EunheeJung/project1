/*
	Project 1
	Name of Project: Super Mario game 
	Author: Eunhee Jung
	Date: 2020.05.29
*/

//PImage [] bg = new PImage[9];
Game game;

void setup()
{
	size(1280, 720);
	game = new Game();
	
}

void draw()
{
	game.updateAndDraw();
}

void mousePressed()
{
	start = false;
}

void keyPressed(KeyEvent e) 
{
	//D = 68, W = 87, A = 65
	if(e.getKeyCode() == 68)
	{
		game.setCheck1(true);
	}

	if(e.getKeyCode() == 65)
	{
		game.setCheck2(true);
	}

	if(e.getKeyCode() == 87)
	{
		game.setCheck3(true);
		
	}


}

void keyReleased(KeyEvent e) {
	if (e.getKeyCode()==68)
	{
		game.setCheck1(false);
	}

	if (e.getKeyCode()==65)
	{
		game.setCheck2(false);
	}
}





public abstract class Elements
{
	protected float x, y ;

	public Elements (float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public void backgroundmove()
	{
		x -= 15; 
	}
	public void drawElement() {}
}

class Pipe extends Elements
{
	Pipe (float x, float y)
	{
		super (x , y);
	}
}

class Item extends Elements
{
	PImage [] flower = new PImage[2];
	PImage mushroom;
	PImage [] star = new PImage[4];

	Item (float x, float y)
	{
		super(x, y);
		
		for (int i =0; i < 2; i++)
		{
			flower[i] = loadImage("items/flower/000"+(i)+".gif");
		}

		mushroom = loadImage("items/mushroom/0000.gif");

		for (int i =0; i < 4; i++)
		{
			star[i] = loadImage("items/star/000"+(i)+".gif");
		}
	}
	void drawElement()
	{
		image(flower[frameCount%2], x, y);
	}

	void drawMushroom()
	{
		image(mushroom, x, y);
	}

	void drawStar()
	{
		image(star[frameCount%4], x, y);
	}

	void elementrDraw()
	{

	}
}

class Block extends Elements
{
	private int MAXELEMENT =100;
	PImage[] blocks = new PImage[MAXELEMENT];
	PImage[] crash = new PImage[33];

	
	Block (float x, float y)
	{
		super (x, y);
		blocks[0] = loadImage("blocks/Block1.gif");
		blocks[1] = loadImage("blocks/Block4.gif");
		blocks[2] = loadImage("blocks/Block100.gif");
		for (int i = 0; i < 33; i++)
		{
			String a = nf(i, 2);
			crash[i] = loadImage("blocks/blockbreak/00"+a+".png");
		}

	}

	public void drawElement()
	{
		imageMode(CENTER);
		image(blocks[1], x, y);
	}


	public void crash()
	{
		imageMode(CENTER);
		image(crash[frameCount%33],x,y);
	}

	public void backgroundmove()
	{
		x -= 15;
	}

	public float getX() {return x;}
	public float getY() {return y;}
}



class Coin extends Elements
{
	PImage[] coin = new PImage[4];

	Coin (float x, float y)
	{
		super(x, y);
		for (int i = 0; i < 4; i++)
		{
			coin[i] = loadImage("coin/000"+(i)+".gif");
		}
	}

	void drawElement()
	{
		imageMode(CENTER);
		image(coin[frameCount%4], x, y);
	}

	public void backgroundmove()
	{
		x -= 15;
	}

	float getX() {return x;}
	float getY() {return y;}
}

class Factory
{
	Block getBlock(float x, float y)
	{
		Block b = new Block(x, y);
		return b;
	}

	Block getQblock(float x, float y)
	{
		Block b = new Block(x, y);
		return b;
	}

	Block getSblock(float x, float y)
	{
		Block b = new Block(x, y);
		return b; 
	}

	Item getFlower(float x, float y)
	{
		Item f = new Item(x, y);
		return f;
	}

	Item getMushroom(float x, float y)
	{
		Item m = new Item(x, y);
		return m;
	}

	Item getStar(float x, float y)
	{
		Item s = new Item(x, y);
		return s;
	}

	Coin getCoin(float x, float y)
	{
		Coin c = new Coin(x, y);
		return c;
	}
}

